var map;
var infowindow;
var latestmarker;
var markerimage;
var html;
var kal;
var d = new Date();
var curr_date = d.getDate();
var curr_month = d.getMonth();
curr_month++;
var curr_year = d.getFullYear();
var today = curr_month + "/" + curr_date + "/" + curr_year;


//Initialize ajetaan kun alustetaan Google Maps -karttapohja.
function initialize() {
  //Mihin kartta keskitetään ja miten paljon zoomattuna
  var myLatLng = new google.maps.LatLng(60.193153, 24.922828);
  var myOptions = {
    zoom: 12,
    center: myLatLng,
    //HYBRID näyttää tiet satelliittikuvan päällä
    mapTypeId: google.maps.MapTypeId.HYBRID
  };
  
  //luodaan markerikuva
  markerimage = new google.maps.MarkerImage('images/Marker_small.png');
  
  //luodaan kartta
  map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
  
  //luodaan uusi infoikkuna jonka sisältö ja sijainti muutetaan sitten tarpeen mukaan
  infowindow = new google.maps.InfoWindow();
  
  //listeneri jolla määritellään mitä tehdään kun käyttäjä sulkee infoikkunan
  google.maps.event.addListener(infowindow, 'closeclick', closeMarker);
  
  //mahdollistetaan kalenterin käyttö
  google.maps.event.addListener(infowindow, 'domready', function(){
  openKalendae();
  });
  
  // TODO vaihda tämä ja muut formit jade-leiskoiksi.
  /* LEGACY
  html =  "<form id='infowindow' method='post' action='/'>" + 
                "<input type = 'text' name='name' />" + "<br />" + 
                "<input type = 'text' name='location' />" + "<br />" + 
                "<input type = 'submit' value='Save' />";
  infowindow.setContent(html);
  */
  
  //kun klikkaa kartasta tyhjää kohtaa luodaan kohdalle uusi kuuntelija ja lisätään kohtaan markeri:
  google.maps.event.addListener(map, 'click', function(event) {
    placeMarker(event.latLng, map);
  });
}

//Funktio luo uuden markkerin, antaa sille eventin tiedot ja sijoittaa sen kartalle oikeaa kohtaan.
function placeMarker(event, map) {
  //jos kyseessä on jo olemassa oleva eventti, sijoitetaan eventin tiedot markkeriin
  if(event.id != null) {
    var location = new google.maps.LatLng(event.x,event.y);
    this.event_ = event;
    this.id_ = event.id;
    this.location_ = location;
    this.map_ = map; 
    var marker = new google.maps.Marker({  
      position: location, 
      map: map,
      icon: markerimage,
      id: event.id,
      event: event
    });
    latestmarker = marker;
  }
  //jos kyseessä ei ole olemassa oleva eventti luodaan kohtaan uusi tyhjä markeri.
  else {
     //Jos käyttäjä on avannut uuden tapahtuman mutta ei tallentanut sitä,
     //vanha markeri tulee poistaa ennen uuden sijoittamista
  	if (!latestmarker.id) {
  	   latestmarker.setMap(null);
  	}
    var location = event;
    this.location_ = location;
    this.map_ = map; 
    var marker = new google.maps.Marker({  
      id: null,
      position: location, 
      map: map,
      icon: markerimage
    });
  
    //Tämä html näytetään kun käyttäjä luo uuden tapahtuman
    var html = 

                 "<form id='infowindow' method='post' action='/new'>" + 
                 "<input type = 'hidden' name='x' value='"+event.lat()+"'>" + 
                 "<input type = 'hidden' name='y' value='"+event.lng()+"'>" + 
                 "<table><tr><td><p class=eventsubheader> Tapahtuman nimi:</td>" + "<td><input type = 'text' name='name'></td></tr>" +
                 "<tr><td><p class=eventsubheader>Päivämäärä:</td>" + "<td><input type='text' name='date' value="+today+" id='input2'></td></tr>" +
	             "<tr><td><p class=eventsubheader>Aloitusaika:</td>"+"<td><input type = 'text' name='time' value='hh:mm'/></td></tr>" + 
	             "<tr><td><p class=eventsubheader>Järjestäjä:</td>"+"<td><input type = 'text' name='creator'/></td></tr>" + 
	             "<tr><td><p class=eventsubheader>Paikka:</td>"+"<td><input type = 'text' name='place'/></td></tr>" +
				//"<tr><td><p class=eventsubheader>Kuvaus:</td>"+"<td><input type = 'text' name='desc'/></td></tr>" +
	        	 "<tr><td><p class=eventsubheader>Vapaa kuvaus:</td>"+"<td><textarea name='desc' cols='30' rows='4'/></textarea></td></tr>" + 
                 "<tr><td><p class=eventsubheader>Tyyppi:</td>"+"<td><select name='type'>" +
                 "<option value='Jalkapallo' SELECTED>Jalkapallo</option>"+
                 "<option value='Koripallo'>Koripallo</option>"+
                 "<option value='Jääkiekko'>Jääkiekko</option>"+
                 "<option value='Lasten aktiviteetit'>Lasten aktiviteetit</option>"+
                 "<option value='Muut'>Muut</option></td></tr></table><br \>"+
                 "<input type = 'submit' value='Tallenna' /></form>";
             
    //asetetaan markerille sisältö
    infowindow.setContent(html);
    //avataan markerin sisältö
    infowindow.open(map, marker);
    latestmarker = marker;
  }
  
  //Kun klikkaa olemassaolevaa markkeria:
  google.maps.event.addListener(marker, 'click', function(event) {
    viewMarker(marker, map);
  });
}

//updateContentia käytetään kun katsotaan olemassaolevaa tapahtumaa
google.maps.InfoWindow.prototype.updateContent = function(event) {
  this.event_ = event;
  if(event.id) {
  
  //Tämä html näytetään kun käyttäjä katsoo olemassaolevaa markeria
    var html_ =  "<h2 class=\"eventtitle\">"+this.event_.name+"</h2>" +
    "<p class=\"eventsubheader\">" +this.event_.date + " · " +this.event_.time + " · " + this.event_.place + " · "+ this.event_.type +
    "<p class=\"eventdescription\">"+this.event_.desc + " <b>-"+this.event_.creator+"</b>";
    
    infowindow.setContent(html_);
  }
}

//kun jotain markkeria klikkaa sen infoikkunan avataan
function viewMarker(marker, map) {

  if(marker.id) {
  //Jos olemassaoleva tapahtuma
    infowindow.updateContent(marker.event);
  } else {
  //Jos uusi tapahtuma
    infowindow.setContent(html);
  }
  //siirretään infoikkuna oikeaan kohtaan ja avataan se.
  infowindow.open(map, marker);
  latestmarker = marker;
}

//Luodaan uusi kalenteri
function openKalendae() {
 new Kalendae.Input('input2', {months:1});
}

//Ajetaan kun käyttäjä sulkee infoikkunan
function closeMarker() {
console.log(infowindow.location);
   if (!latestmarker.id) {
      //Jos markeria ei ole tallennettu se tulee poistaa
      latestmarker.setMap(null);
   }
infowindow.close();
}
