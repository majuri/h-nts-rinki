//TODO Siivoamista ja refaktorointia

/**
 * Module dependencies.
 */
var express = require('express')
  , routes = require('./routes')
  , events = require('./events')
  , searchtypes = require('./searchtypes')
  , mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , events = require('./events');
  
var d = new Date();
var curr_date = d.getDate();
var curr_month = d.getMonth();
curr_month++;
var curr_year = d.getFullYear();
var searchdate = curr_month + "/" + curr_date + "/" + curr_year;
var events_ = [];



var app = module.exports = express.createServer();
mongoose.connect('mongodb://h-nts-rinki:hontsarinki@ds031947.mongolab.com:31947/h-nts-rinki');
// Configuration
app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(require('stylus').middleware({ src: __dirname + '/public' }));
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Schemas

var EventSchema = new Schema({
  x: Number,
  y: Number,
  type: String,
  place: String,
  creator: String,
  date: String,
  time: String,
  desc: String,
  name: String,
  id: String
});

Event = mongoose.model('Events', EventSchema);

// Routes

//Get home page route
// TODO Yhdistä events ja event haku järkeväksi kokonaisuudeksi

app.get('/', function (req, res) {
  var searchtypes_ = searchtypes.all;
  var type_ = 'Kaikki';
  var event_;
  events_ = [];
  Event.find({}, function(err, events) {
    if(err) {
      throw err;
      console.log(err);
    } else {
      events.forEach(function(event) {
        //console.log('saved ' + event);
        events_.push(event);
        event_ = event;
      });
      res.render('index.jade', {
        locals: {
          events: events_,
          event: null,
          searchtypes: searchtypes_,
          searchtype_: type_,
          searchdate_: searchdate,
          title: 'Höntsärinki'
        }
      });
    }
  });
});

/* Eventin URL ID:n perusteella toistaiseksi jätetty pois
app.get('/events/:id', function (req, res) {
  var event = events.find(req.params.id);
  var searchtypes_ = searchtypes.all;
  var type_ = searchtypes.find;
  res.render('index.jade', {
    locals: {
      event: event,
      events: null,           
      searchtypes: searchtypes_,
      searchtype_: type_,
      searchdate_: searchdate,
      title: 'Höntsärinki | ' + event.name
    }
  });
});
*/

app.post('/new', function(req, res){
  //kun kartalle lisataan tapahtuma luodaan event ja tallennetaan se
  var searchtypes_ = searchtypes.all;
  var type_ = searchtypes.find;
  var new_ = new Event({
    id: 100,
    name: req.param('name'),
    type: req.param('type'),
    date: req.param('date'),
    place: req.param('place'),
    creator: req.param('creator'),
    time: req.param('time'),
    desc: req.param('desc'),
    x: req.param('x'),
    y: req.param('y')
  });
  events_.push(new_);
  new_.save(function(err) {
    if(err) {console.log('Not saved ' + err);}
    else {
      res.redirect('/');
    }
  });
});

app.post('/', function(req, res) {
  var searchtypes_ = searchtypes.all;
  console.log(req.param('searched_date'));
  searchdate = req.param('searched_date');
  var type_ = req.param('searched_type');
  events_ = [];
  if(req.param('searched_type') === 'Kaikki') {
    Event.find({'date':req.param('searched_date')}, function(err, events) {
      if(err) {
        throw err;
        console.log(err);
      } else {
        events.forEach(function(event) {
          //console.log('saved ' + event);
          events_.push(event);
          event_ = event;
        });
        res.render('index.jade', {
          locals: {
            events: events_,
            event: null,
            searchtypes: searchtypes_,
            searchtype_: type_,
            searchdate_: searchdate,
            title: 'Höntsärinki'
          }
       });
     }
    });
  } else {
    Event.find({'date':req.param('searched_date'), 'type': req.param('searched_type')}, function(err, events) {
        if(err) {
          throw err;
          console.log(err);
        } else {
          events.forEach(function(event) {
            //console.log('saved ' + event);
            events_.push(event);
            event_ = event;
          });
          res.render('index.jade', {
            locals: {
              events: events_,
              event: null,
              searchtypes: searchtypes_,
              searchtype_: type_,
              searchdate_: searchdate,
              title: 'Höntsärinki'
            }
         });
       }
      });
  }
  
})

//Get blog page route
app.get('/blog', function (req, res) {
  res.render('blog.jade', {
    locals: {
      title: 'Höntsärinki | Blog'
     }
  });
});


//Get about page route
app.get('/about', function (req, res) {
  res.render('about.jade', {
    locals: {
      title: 'Höntsärinki | About'
    }
  });
});

app.listen(17025);
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
