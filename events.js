/* Testitapahtumia joita voi käyttää ilman tietokantaa. LEGACY
var events = [
  {
    id: 0,
    name: 'Mikon tapahtuma',
    type: 'Jalkapallo',
    x: 60.162052,
    y: 24.933643,
    date: '05/20/2012'
  },
  {
    id: 1,
    name: 'Jannen tapahtuma',
    type: 'Jalkapallo',
    x: 60.175245,
    y: 24.943256,
    date: '05/21/2012'
  },
  {
    id: 2,
    name: 'Villen tapahtuma',
    type: 'Jalkapallo',
    x: 60.186385,
    y: 24.923772,
    date: '05/22/2012'
  },
  {
    id: 3,
    name: 'Futistapahtuma',
    type: 'Jalkapallo',
    x: 60.172129,
    y: 24.917249,
    date: '05/23/2012'
  },
  {
    id: 4,
    name: 'Toinen futistapahtuma',
    type: 'Jalkapallo',
    x: 60.176782,
    y: 24.920854,
    date: '05/24/2012'
  }
];
*/

//module.exports.all = events;

module.exports.find = function(id) {
  id = parseInt(id, 10);
  var found = null;
  eventLoop: for (event_index in events) {
    var event = events[event_index];
    if (event.id === id) {
      found = event;
      break eventLoop;
    }
  };
  return found;
}

module.exports.save = function(name, x, y, type, date) {
  events.push({id: events.length, name: name, x: x, y:y, type:type, date:date, place:place, creator:creator, time:time, desc:desc});
}


